# README #

* Pasos efectuados para la tarea.

### What is this repository for? ###

* Repositorio para la tarea de entornos

### How do I get set up? ###

* cd d 
* ir al directorio D
* mkdir test_repository
* crear una carpeta en D
* cd test_repository
* para movernos dentro de esa carpeta
* git config --global user.name ""
* para cambiar la configuracion de usuario
* git config --global user.email ""
* para cambiar la configuracion del email del usuario
* git config --list
* para ver la configuracion 
* git init
* para inicilizar un repositorio vacío en nuestro directorio
* git clone https://carolentornos@bitbucket.org/carolentornos/test_repository.git
* clonamos el repositorio
* hacemos git add para actualizarlo
* git commit -m ""
* para hacer commit con un comentario
* push origin main
* confirmamos que está todo bien con push origin main